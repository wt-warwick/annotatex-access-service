const {ApolloServer} = require('apollo-server-express');
const context        = require('./context');
const express        = require('express');
const fs             = require('fs');
const process        = require('process');
const resolvers      = require('./resolvers');
const typeDefs       = require('./schema');

const app    = express();
const port   = process.env.PORT || 4000;
const server = new ApolloServer({typeDefs, resolvers, context});

server.applyMiddleware({app});

app.listen({port}, () => {
    console.log(`http://localhost:${port}${server.graphqlPath}`);
});
