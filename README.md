# annotatex-access-service
A [GraphQL](https://graphql.org/) service for accessing AnnotateX data.

## Build
To build the project type:

```
npm install
```

This will download all the dependencies.

## Run
The idea behind the service is to expose annotation data to other users. Backup the MySQL database
in `apmup.lnx.warwick.ac.uk` and import it in a local database.

The service needs some environment variables to be set before its execution (be sure the values match
your database configuration):

```
export MYSQL_HOST=localhost
export MYSQL_PORT=3306
export MYSQL_USER=root
export MYSQL_PASS=root
export MYSQL_DB=dbjoomla
```

Be aware that as soon as you close the current session these variables will be lost. Consider adding
them to your `.bashrc` or write a `build.sh` script for your needs.

To run the project type:

```
nodejs index.js
```

## Accessing the data
To access the data you need a GraphQL client such as Altair GraphQL Client.
In the client make sure to type the correct address for the GraphQL server, e.g.
`http://localhost:4000/graphql`.

You can check everything is all right by typing the following query:

```
{
  users {
    username
  }
}
```

## To Dos
There's an initial Dockerfile, but it's not fully working. It would be nice to finish it.
