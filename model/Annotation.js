module.exports = class Annotation {
    constructor(id, sentence, labels) {
	this.id = id;
	this.sentence = sentence;
	this.labels = labels;
    }
};
