module.exports = class Submission {
    constructor(id, loadTime, submitTime) {
	this.id = id;
	this.loadTime = loadTime;
	this.submitTime = submitTime;
    }
}
