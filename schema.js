const { gql } = require('apollo-server-express');

module.exports = gql`
  type Query {
    annotations(offset: Int, limit: Int): [Annotation]
    annotationTotalCount: Int    
    report(id: String!): Report
    reportTotalCount: Int
    reports(offset: Int, limit: Int): [Report]
    submission(id: Int!): Submission
    submissionTotalCount: Int
    submissions(user: String, offset: Int, limit: Int): [Submission]
    user(username: String!): User
    userTotalCount: Int
    users(offset: Int, limit: Int): [User]
  },

  type Annotation {
    sentence: String
    labels: [String]
    report: Report
    submission: Submission
    user: User
  }

  type Report {
    id: String
    content: String
  },

  type Submission {
    annotationTotalCount: Int
    annotations(offset: Int, limit: Int): [Annotation]
    id: String
    loadTime: String
    report: Report
    submitTime: String
    user: User
  },

  type User {
    annotations(offset: Int, limit: Int): [Annotation]
    annotationTotalCount: Int
    username: String
    name: String
    submissionTotalCount: Int
    submissions(offset: Int, limit: Int): [Submission]
  },
`;
