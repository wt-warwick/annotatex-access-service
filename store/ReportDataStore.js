const Report = require('../model/Report');

module.exports = class ReportDataStore {
    constructor(connection) {
	this.connection = connection;
    };

    get(offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT id      AS id,
                       content AS content
                FROM ax_report
                ORDER BY id
                LIMIT ${limit}
                OFFSET ${offset};`, (error, rows) => {
	            if (error) {
			reject(error);
		    } else if (rows.length) {
			resolve(rows.map(row => new Report(row.id, row.content)));
		    } else {
			resolve(null);
		    }
		});
	});		
    }

    getCount() {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT COUNT(*) AS total_count
                FROM ax_report;`, (error, rows) => {
	            if (error) {
			reject(error);
		    } else {
			resolve(rows[0].total_count);
		    }
		});
	});
    }

    getByAnnotation(annotation) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT ax_report.id      AS id,
                       ax_report.content AS content
                FROM ax_annotation
                INNER JOIN ax_submission
                ON ax_annotation.submission = ax_submission.id
                INNER JOIN ax_report
                ON ax_submission.report = ax_report.id
                WHERE ax_annotation.id = ?;`, [annotation.id], (error, rows) => {
		    if (error) {
			reject(error);
		    } else {
			resolve(new Report(rows[0].id, rows[0].content));
		    }
		});
	});
    }

    getById(id) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT id      AS id,
                       content AS content
                FROM ax_report
                WHERE id = ?`, [id], (error, rows) => {
	            if (error) {
			reject(error);
		    } else if (rows.length) {
			resolve(new Report(rows[0].id, rows[0].content));
		    } else {
			resolve(null);
		    }
		});
	});		
    }

    getBySubmission(submission) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT ax_report.id      AS id,
                       ax_report.content AS content
                FROM ax_submission
                INNER JOIN ax_report
                ON ax_submission.report = ax_report.id
                WHERE ax_submission.id = ?`, [submission.id], (error, rows) => {
		    if (error) {
			reject(error);
		    } else if (!rows.length) {
			resolve(null);
		    } else {
			resolve(new Report(rows[0].id, rows[0].content));
		    }
		});
	});
    }
};
