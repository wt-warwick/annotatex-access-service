const Annotation = require('../model/Annotation');

module.exports = class AnnotationDataStore {
    constructor(pool) {
	this.pool = pool;
    }

    get(offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.pool.query(`
                SELECT annotation.id                                                              AS id,
                       annotation.normalized_sentence                                             AS sentence,
                       GROUP_CONCAT(DISTINCT annotation_label.label ORDER BY label SEPARATOR '#') AS labels
                FROM ax_submission AS submission
                INNER JOIN ax_annotation AS annotation
                ON submission.id = annotation.submission
                INNER JOIN ax_annotation_label AS annotation_label
                ON annotation.id = annotation_label.annotation
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.user = submission.user
                    AND ax_submission.report = submission.report)
                GROUP BY annotation.id,
                         annotation.normalized_sentence
                LIMIT ${limit}
                OFFSET ${offset};`, (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve([]);
		} else {
		    resolve(rows.map(row => new Annotation(row.id, row.sentence, row.labels.split('#'))));
		}
	    });
	});
    }

    getCount() {
	return new Promise((resolve, reject) => {
	    this.pool.query(`
                SELECT COUNT(*) AS total_count
                FROM ax_submission AS submission
                INNER JOIN ax_annotation AS annotation
                ON submission.id = annotation.submission
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.user = submission.user
                    AND ax_submission.report = submission.report);`, (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve([]);
		} else {
		    resolve(rows[0].total_count);
		}
	    });
	});
    }

    getBySubmission(submission, offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.pool.query(`
                SELECT annotation.id                                                              AS id,
                       annotation.normalized_sentence                                             AS sentence,
                       GROUP_CONCAT(DISTINCT annotation_label.label ORDER BY label SEPARATOR '#') AS labels
                FROM ax_submission AS submission
                INNER JOIN ax_annotation AS annotation
                ON submission.id = annotation.submission
                INNER JOIN ax_annotation_label AS annotation_label
                ON annotation.id = annotation_label.annotation
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.user = submission.user
                    AND ax_submission.report = submission.report)
                AND submission.id = ?
                GROUP BY annotation.id,
                         annotation.normalized_sentence
                LIMIT ${limit}
                OFFSET ${offset};`, [submission.id], (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve([]);
		} else {
		    resolve(rows.map(row => new Annotation(row.id, row.sentence, row.labels.split('#'))));
		}
	    });
	});
    }

    getBySubmissionCount(submission) {
	return new Promise((resolve, reject) => {
	    this.pool.query(`
                SELECT COUNT(*) AS total_count
                FROM ax_submission AS submission
                INNER JOIN ax_annotation AS annotation
                ON submission.id = annotation.submission
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.user = submission.user
                    AND ax_submission.report = submission.report)
                AND submission.id = ?;`, [submission.id], (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve([]);
		} else {
		    resolve(rows[0].total_count);
		}
	    });
	});
    }

    getByUser(user, offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.pool.query(`
                SELECT annotation.id                                                              AS id,
                       annotation.normalized_sentence                                             AS sentence,
                       GROUP_CONCAT(DISTINCT annotation_label.label ORDER BY label SEPARATOR '#') AS labels
                FROM ax_submission AS submission
                INNER JOIN ax_annotation AS annotation
                ON submission.id = annotation.submission
                INNER JOIN ax_annotation_label AS annotation_label
                ON annotation.id = annotation_label.annotation
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.user = submission.user
                    AND ax_submission.report = submission.report)
                AND submission.user = ?
                GROUP BY annotation.id,
                         annotation.normalized_sentence
                LIMIT ${limit}
                OFFSET ${offset};`, [user.id], (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve([]);
		} else {
		    resolve(rows.map(row => new Annotation(row.id, row.sentence, row.labels.split('#'))));
		}
	    });
	});
    }

    getByUserCount(user) {
	return new Promise((resolve, reject) => {
	    this.pool.query(`
                SELECT COUNT(*) AS total_count
                FROM ax_submission AS submission
                INNER JOIN ax_annotation AS annotation
                ON submission.id = annotation.submission
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.user = submission.user
                    AND ax_submission.report = submission.report)
                AND submission.user = ?;`, [user.id], (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve([]);
		} else {
		    resolve(rows[0].total_count);
		}
	    });
	});
    }
};
