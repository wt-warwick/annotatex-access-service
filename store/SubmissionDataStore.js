const Submission = require('../model/Submission');

module.exports = class SubmissionDataStore {
    constructor(connection) {
	this.connection = connection;
    }

    get(offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT submission.id          AS id,
                       submission.load_time   AS load_time,
                       submission.submit_time AS submit_time
                FROM ax_submission AS submission
                INNER JOIN jmt_users AS user
                ON submission.user = user.id
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.report = submission.report
                    AND ax_submission.user = submission.user)
                ORDER BY submission.submit_time
                LIMIT ${limit}
                OFFSET ${offset};`, (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(rows.map(row => new Submission(row.id, row.load_time, row.submit_time)));
		}
	    });
	});
    }

    getCount() {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT COUNT(DISTINCT user, report) AS total_count
                FROM ax_submission AS submission
                WHERE submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.report = submission.report
                    AND ax_submission.user = submission.user);`, (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(rows[0].total_count);
		}
	    });
	});
    }

    getByAnnotation(annotation) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT submission.id          AS id,
                       submission.load_time   AS load_time,
                       submission.submit_time AS submit_time
                FROM ax_annotation AS annotation
                INNER JOIN ax_submission AS submission
                ON annotation.submission = submission.id;`, (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(new Submission(rows[0].id, rows[0].load_time, rows[0].submit_time));
		}
	    });
	});
    }

    getByUser(user, offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT submission.id          AS id,
                       submission.load_time   AS load_time,
                       submission.submit_time AS submit_time
                FROM ax_submission AS submission
                INNER JOIN jmt_users AS user
                ON submission.user = user.id
                WHERE user.username = ?
                AND submission.submit_time = (
                    SELECT MAX(submit_time)
                    FROM ax_submission
                    WHERE ax_submission.report = submission.report
                    AND ax_submission.user = submission.user)
                ORDER BY submission.submit_time
                LIMIT ${limit}
                OFFSET ${offset};`, [user.username], (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(rows.map(row => new Submission(row.id, row.load_time, row.submit_time)));
		}
	    });
	});
    }

    getByUserCount(user) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT COUNT(DISTINCT report) AS total_count
                FROM ax_submission
                INNER JOIN jmt_users
                ON ax_submission.user = jmt_users.id
                WHERE jmt_users.username = ?;`, [user.username], (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(rows[0].total_count);
		}
	    });
	});
    }
};
