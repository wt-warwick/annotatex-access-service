const User = require('../model/User');

module.exports = class UserDataStore {
    constructor(connection) {
	this.connection = connection;
    }

    get(offset = 0, limit = 64) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT id,
                       username,
                       name
                FROM jmt_users
                ORDER BY username
                LIMIT ${limit}
                OFFSET ${offset};`, (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(rows.map(row => new User(row.id, row.username, row.name)));
		}
	    });
	});
    }

    getCount() {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT COUNT(*) AS total_count;
                FROM jmt_users;`, (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    resolve(rows[0].total_count);
		}
	    });
	});
    }
    
    getByAnnotation(annotation) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT jmt_users.id,
                       jmt_users.username,
                       jmt_users.name
                FROM ax_annotation
                INNER JOIN ax_submission
                ON ax_annotation.submission = ax_submission.id
                INNER JOIN jmt_users
                ON ax_submission.user = jmt_users.id
                WHERE ax_annotation.id = ?;`, [annotation.id], (error, rows) => {
		    if (error) {
			reject(error);
		    } else {
			resolve(new User(rows[0].id, rows[0].username, rows[0].name));
		    }
		});
	});
    }

    getBySubmission(submission) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT id,
                       username,
                       name
                FROM jmt_users
                INNER JOIN ax_submission
                ON jmt_users.id = ax_submission.user
                WHERE ax_submission.id = ?;`, [submission.id], (error, rows) => {
                if (error) {
		    reject(error);
		} else if (!rows.length) {
		    resolve(null);
		} else {
		    resolve(new User(rows[0].id, rows[0].username, rows[0].name));
		}
	    });
	});
    }

    getByUsername(username) {
	return new Promise((resolve, reject) => {
	    this.connection.query(`
                SELECT id,
                       username,
                       name
                FROM jmt_users
                WHERE username = ?;`, [username], (error, rows) => {
                if (error) {
		    reject(error);
		} else {
		    if (rows.length) {
			resolve(new User(rows[0].id, rows[0].username, rows[0].name));
		    } else {
			resolve(null);
		    }
		}
	    });
	});
    }
};
