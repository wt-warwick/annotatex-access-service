module.exports = {
    Query: {
	annotations: (parent, args, context, info) => {
	    return context.annotations.get(args.offset, args.limit);
	},
	annotationTotalCount: (parent, args, context, info) => {
	    return context.annotations.getCount();
	},
	report: (parent, args, context, info) => {
	    return context.reports.getById(args.id);
	},
	reportTotalCount: (parent, args, context, info) => {
	    return context.reports.getCount();
	},
	reports: (parent, args, context, info) => {
	    return context.reports.get(args.offset, args.limit);
	},
	submission: (parent, args, context, info) => {
	    return context.submissions.getById(args.id);
	},
	submissionTotalCount: (parent, args, context, info) => {
	    return context.submissions.getCount();
	},
	submissions: (parent, args, context, info) => {
	    return context.submissions.get(args.offset, args.limit);
	},
	user: (parent, args, context, info) => {
	    return context.users.getByUsername(args.username);
	},
	userTotalCount: (parent, args, context, info) => {
	    return context.users.getCount();
	},
	users: (parent, args, context, info) => {
	    return context.users.get(args.offset, args.limit);
	},
    },
    Annotation: {
	report: (parent, args, context, info) => {
	    return context.reports.getByAnnotation(parent);
	},
	submission: (parent, args, context, info) => {
	    return context.submissions.getByAnnotation(parent);
	},
	user: (parent, args, context, info) => {
	    return context.users.getByAnnotation(parent);
	},
    },
    Submission: {
	annotations: (parent, args, context, info) => {
	    return context.annotations.getBySubmission(parent, args.offset, args.limit);
	},
	annotationTotalCount: (parent, args, context, info) => {
	    return context.annotations.getBySubmissionCount(parent);
	},
	report: (parent, args, context, info) => {
	    return context.reports.getBySubmission(parent);
	},
	user: (parent, args, context, info) => {
	    return context.users.getBySubmission(parent);
	},
    },
    User: {
	annotationTotalCount: (parent, args, context, info) => {
	    return context.annotations.getByUserCount(parent);
	},
	annotations: (parent, args, context, info) => {
	    return context.annotations.getByUser(parent, args.offset, args.limit);
	},
	submissionTotalCount: (parent, args, context, info) => {
	    return context.submissions.getByUserCount(parent);
	},
	submissions: (parent, args, context, info) => {
	    return context.submissions.getByUser(parent, args.offset, args.limit);
	},
    },
};
