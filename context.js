const mysql = require('mysql');
const process = require('process');
const AnnotationDataStore = require('./store/AnnotationDataStore');
const ReportDataStore = require('./store/ReportDataStore');
const SubmissionDataStore = require('./store/SubmissionDataStore');
const UserDataStore = require('./store/UserDataStore');
const {
    MYSQL_HOST,
    MYSQL_PORT,
    MYSQL_USER,
    MYSQL_PASS,
    MYSQL_DB,
} = process.env;

const pool = mysql.createPool({
	connectionLimit : 10,
	host            : MYSQL_HOST,
        port            : MYSQL_PORT,
	user            : MYSQL_USER,
	password        : MYSQL_PASS,
	database        : MYSQL_DB,
});

module.exports = ({ req, res }) => {
	return new Promise((resolve, reject) => {
		pool.getConnection((err, conn) => {
			if (err) {
				reject(err);
			} else {
				resolve(conn);
			}
		});
	}).then(conn => {
		return new Promise((resolve, reject) => {
			conn.beginTransaction(err => {
				if (err) {
					reject(err);
				} else {
					resolve(conn);
				}
			});
		});
	}).then(conn => {
		const end = res.end;

		res.end = function() {
			conn.commit(err => {
				if (err) {
					console.err(err);
				}

				conn.release();
				end.apply(this, arguments);
			});
		};

		return {
			annotations: new AnnotationDataStore(conn),
			reports: new ReportDataStore(conn),
			submissions: new SubmissionDataStore(conn),
			users: new UserDataStore(conn),
		};
	});
};
